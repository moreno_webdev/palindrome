var palindrome = (function () {

    var racecar = '',
      carrace = '';

    return {
      set: function (val) {
        // Remove whitespace and change case
        racecar = val.replace(/\W/g, '').toLowerCase();
        // Reverse string
        carrace = racecar.split('').reverse().join('');
      },
      isPalindrome: function () {
        return (racecar === carrace);
      }
    };
  }());

$(document).ready(function () {

  $('#palindrome').submit(function (event) {

    event.preventDefault();

    var that = $(this);

    // Grab text field value and set
    palindrome.set($('input[type="text"]').val());
    that.next().remove();
    if (palindrome.isPalindrome()) {
      $('<p>this <strong>IS</strong> a palindrome</p>').insertAfter(that).fadeIn('slow');
    } else {
      $('<p><strong>NOT</strong> a palindrome</p>').insertAfter(that).fadeIn('slow');
    }
  });
});